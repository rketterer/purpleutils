﻿using System;

namespace Bombora.Integrations.Sauron.Models.Entities
{
    public class UserShort
    {
        public int? Id { get; set; }
        public String Email { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public AccountInfo Account { get; set; }

        //public bool Validate()
        //{
        //    Regex regex = new Regex(VALID_EMAIL_REGEX);

        //    return (Email != null)
        //            && Email.Length >= 4
        //            && regex.Match(Email).Success
        //            && (FirstName == null || FirstName.Length <= 100)
        //            && (LastName == null || LastName.Length <= 100);
        //}
    }
}
