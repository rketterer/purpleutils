﻿namespace Bombora.Integrations.Sauron.Models.Entities
{
    public class UserRole
    {
        // This service was originally implemented in Ruby on Rails
        // All fields are lower case for backward compatability
        public class Fields
        {
            public const string RoleId = "_id";
            public const string EntityId = "eid";
        }

        public int UserId { get; set; }
        public int RoleId { get; set; }
        public int EntityId { get; set; }
    }
}
