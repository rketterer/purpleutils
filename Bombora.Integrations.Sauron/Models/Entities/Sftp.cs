﻿using System.Runtime.Serialization;

namespace Bombora.Integrations.Sauron.Models.Entities
{
    [DataContract]
    public class Sftp
    {
        public class Fields
        {
            public const string REMOTE_DIR = "rmdir";
            public const string URL = "url";
            public const string USERNAME = "uname";
            public const string PASSWORD = "pwd";
            public const string IV = "iv";
            public const string PORT = "port";
        }
        [DataMember]
        //[BsonElement(Fields.REMOTE_DIR)]
        //[BsonIgnoreIfNull]
        public string RemoteDir { get; set; }

        [DataMember]
        //[BsonElement(Fields.URL)]
        //[BsonIgnoreIfNull]
        public string Url { get; set; }

        [DataMember]
        //[BsonElement(Fields.PORT)]
        //[BsonIgnoreIfNull]
        public int? Port { get; set; }

        [DataMember]
        //[BsonElement(Fields.USERNAME)]
        //[BsonIgnoreIfNull]
        public string Username { get; set; }

        [DataMember]
        //[BsonElement(Fields.PASSWORD)]
        //[BsonIgnoreIfNull]
        public string Password { get; set; }

        [DataMember]
        //[BsonElement(Fields.IV)]
        //[BsonIgnoreIfNull]
        public string IV { get; set; }
    }
}
