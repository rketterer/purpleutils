﻿using System;
using System.Collections.Generic;

namespace Bombora.Integrations.Sauron.Models.Entities
{
    public class Entity
    {
        public int? Id { get; set; }
        public String Name { get; set; }
        public int? ParentId { get; set; }
        public bool? IsAdvertiser { get; set; }
        public bool? IsPublisher { get; set; }
        public bool? IsAgency { get; set; }
        public bool? IsMidMarket { get; set; }
        public int? DefaultListId { get; set; }
        public String EncryptionKey { get; set; }
        public double? Priority { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public String Type { get; set; }

        public IEnumerable<Pricing> Pricings { get; set; }

        public IEnumerable<Product> Products { get; set; }

        public IEnumerable<int> TopicsWhitelist { get; set; }

        public IEnumerable<string> DataExWhitelist { get; set; }

        public IEnumerable<Label> Labels { get; set; }
    }
}
