﻿using System.Collections.Generic;

namespace Bombora.Integrations.Sauron.Models.Entities
{
    public class Role
    {
        public int RoleId { get; set; }
        public IEnumerable<int> EntityIds { get; set; }
    }
}
