﻿namespace Bombora.Integrations.Sauron.Models.Entities
{
    public class Label
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Color { get; set; }
    }
}
