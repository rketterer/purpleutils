﻿using System.Runtime.Serialization;

namespace Bombora.Integrations.Sauron.Models.Entities
{
    [DataContract]
    public class Notification
    {
        public class Fields
        {
            public const string Email = "email";
            public const string Type = "type";
        }

        [DataMember]
        //[BsonElement(Fields.Email)]
        //[BsonIgnoreIfNull]
        public string Email { get; set; }

        [DataMember]
        //[BsonElement(Fields.Type)]
        //[BsonIgnoreIfNull]
        public string Type { get; set; }
    }
}
