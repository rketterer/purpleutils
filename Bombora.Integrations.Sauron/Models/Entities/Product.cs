﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Bombora.Integrations.Sauron.Models.Entities
{
    [DataContract]
    //[BsonIgnoreExtraElements]
    public class Product
    {
        #region constants
        public const string SURGE = "surge";
        public const string DSB = "dsb";
        public const string CONTENT_INSIGHTS = "content_insights";
        public const string ESB = "esb_frog";
        public const string AUDIENCE_VERIFICATION = "aud_verification";
        public const string VISITOR_INSIGHTS = "visitor_insights";
        public const string FIREHOSE = "firehose";
        public const string IPDENTIFIER = "iptocompany";
        #endregion

        public class Fields
        {
            public const string ID = "_id";
            public const string Name = "nm";
            public const string Weekly = "weekly";
            public const string Enabled = "enabled";
            public const string DataExchange = "dex";
            public const string ExposedIntent = "exposed_intent";
            public const string MaxCompanies = "max_comp";
            public const string MaxTopics = "max_topics";
            public const string MaxContacts = "max_contacts";
            public const string MaxCategories = "max_categories";
            public const string MaxSections = "max_sections";
            public const string MaxTracks = "max_tracks";
            public const string EntityParty = "e_party";
            public const string StartDate = "start_dt";
            public const string EndDate = "end_dt";
            public const string MonthlyCost = "monthly_cost";
            public const string Custom = "cust";
            public const string File = "file";
            public const string Sftp = "sftp";
            public const string S3 = "s3";
            #region iptocompany
            public const string Balance = "balance";
            public const string Notifications = "notifications";
            #endregion
            public const string MarketoEnabled = "marketo_enabled";
            public const string LinkedInEnabled = "linkedin_enabled";
            public const string DownloadDisabled = "download_disabled";
        }


        [DataMember]
        //[BsonId]
        public String Id { get; set; }

        [DataMember]
        //[BsonElement(Fields.Name)]
        //[BsonIgnoreIfNull]
        public String Name { get; set; }

        [DataMember]
        //[BsonElement(Fields.Weekly)]
        //[BsonIgnoreIfNull]
        public bool? Weekly { get; set; }

        [DataMember]
        //[BsonElement(Fields.Enabled)]
        //[BsonIgnoreIfNull]
        public bool? Enabled { get; set; }

        [DataMember]
        //[BsonElement(Fields.DataExchange)]
        //[BsonIgnoreIfNull]
        public String DataExchange { get; set; }

        [DataMember]
        //[BsonElement(Fields.MaxCompanies)]
        //[BsonIgnoreIfNull]
        public int? MaxCompanies { get; set; }

        [DataMember]
        //[BsonElement(Fields.MaxContacts)]
        //[BsonIgnoreIfNull]
        public int? MaxContacts { get; set; }

        [DataMember]
        //[BsonElement(Fields.MaxTopics)]
        //[BsonIgnoreIfNull]
        public int? MaxTopics { get; set; }

        [DataMember]
        //[BsonElement(Fields.ExposedIntent)]
        //[BsonIgnoreIfNull]
        public bool? ExposedIntent { get; set; }

        [DataMember]
        //[BsonElement(Fields.MaxCategories)]
        //[BsonIgnoreIfNull]
        public int? MaxCategories { get; set; }

        [DataMember]
        //[BsonElement(Fields.MaxSections)]
        //[BsonIgnoreIfNull]
        public int? MaxSections { get; set; }

        [DataMember]
        //[BsonElement(Fields.EntityParty)]
        //[BsonIgnoreIfNull]
        public IEnumerable<int> EntityParty { get; set; }

        [DataMember]
        //[BsonElement(Fields.StartDate)]
        //[BsonIgnoreIfNull]
        public DateTime? StartDate { get; set; }

        [DataMember]
        //[BsonElement(Fields.EndDate)]
        //[BsonIgnoreIfNull]
        public DateTime? EndDate { get; set; }

        [DataMember]
        //[BsonElement(Fields.MonthlyCost)]
        //[BsonIgnoreIfNull]
        public float? MonthlyCost { get; set; }

        [DataMember]
        //[BsonElement(Fields.Custom)]
        //[BsonIgnoreIfNull]
        public bool? Custom { get; set; }

        [DataMember]
        //[BsonElement(Fields.File)]
        //[BsonIgnoreIfNull]
        public string File { get; set; }

        [DataMember]
        //[BsonElement(Fields.Sftp)]
        //[BsonIgnoreIfNull]
        public Sftp Sftp { get; set; }

        [DataMember]
        //[BsonElement(Fields.S3)]
        //[BsonIgnoreIfNull]
        public S3 S3 { get; set; }

        //[BsonIgnore]
        public string StartDateStr { get; set; }
        //[BsonIgnore]
        public string EndDateStr { get; set; }

        [DataMember]
        //[BsonElement(Fields.Balance)]
        //[BsonIgnoreIfNull]
        public int? Balance { get; set; }

        [DataMember]
        //[BsonElement(Fields.Notifications)]
        //[BsonIgnoreIfNull]
        public IEnumerable<Notification> Notifications { get; set; }

        [DataMember]
        //[BsonElement(Fields.MarketoEnabled)]
        //[BsonIgnoreIfNull]
        public bool? MarketoEnabled { get; set; }

        [DataMember]
        //[BsonElement(Fields.LinkedInEnabled)]
        //[BsonIgnoreIfNull]
        public bool? LinkedInEnabled { get; set; }

        [DataMember]
        //[BsonElement(Fields.DownloadDisabled)]
        //[BsonIgnoreIfNull]
        public bool? DownloadDisabled { get; set; }

        [DataMember]
        //[BsonElement(Fields.MaxTracks)]
        //[BsonIgnoreIfNull]
        public int? MaxTracks { get; set; }
    }
}
