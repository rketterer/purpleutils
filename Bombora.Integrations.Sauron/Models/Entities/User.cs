﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bombora.Integrations.Sauron.Models.Entities
{
    public class User
    {
        private const String VALID_EMAIL_REGEX = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";

        public class Fields
        {
            public const string ID = "_id";
            public const string EntityId = "eid";
            public const string FirstName = "fn";
            public const string LastName = "ln";
            public const string Email = "email";
            public const string Password = "pwd";
            public const string IsAdmin = "ml_admin";
            public const string CreateDate = "cdt";
            public const string UpdateDate = "udt";
            public const string UserRoles = "roles";
            public const string UserRolesByEntity = "roles.eid";
            public const string ApiRoles = "apis";
            public const string FailedAttempts = "failed_attempts";
            public const string LockedOut = "locked_out";
            public const string EmailOptOut = "email_opt_out";
        }


        // This service was originally implemented in Ruby on Rails
        // All fields are lower case for backward compatability

        public int? Id { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool? IsAdmin { get; set; }
        public int? EntityId { get; set; }
        public int? FailedAttempts { get; set; }
        public bool? LockedOut { get; set; }
        public bool? EmailOptOut { get; set; }
        public IEnumerable<UserRole> UserRoles { get; set; }
        public IEnumerable<int> ApiRoles { get; set; }

        //public bool Validate()
        //{
        //    Regex regex = new Regex(VALID_EMAIL_REGEX);

        //    return (Email != null)
        //            && Email.Length >= 4
        //            && regex.Match(Email).Success
        //            && (FirstName == null || FirstName.Length <= 100)
        //            && (LastName == null || LastName.Length <= 100);
        //}

        //public static String HashPassword(String password)
        //{
        //    return BCrypt.Net.BCrypt.HashPassword(password);
        //}

        //public static bool VerifyPassword(String password, String hash)
        //{
        //    return BCrypt.Net.BCrypt.Verify(password, hash);
        //}
    }
}
