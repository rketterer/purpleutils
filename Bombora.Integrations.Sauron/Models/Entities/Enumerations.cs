﻿using System.ComponentModel.DataAnnotations;

namespace Bombora.Integrations.Sauron.Models.Entities
{
    public enum EnumActions
    {
        [Display(Name = "Log-in")]
        LogIn = 1,
        [Display(Name = "New Surge Report")]
        SurgeCreateReport = 2,
        [Display(Name = "New Display Segment")]
        DSBCreateSegment = 3,
        [Display(Name = "New User")]
        CreateUser = 4,
        [Display(Name = "Log-out")]
        LogOut = 5,
        [Display(Name = "Modified User")]
        ModifyUser = 6,
        [Display(Name = "Modifed Display Segment")]
        DSBModifySegment = 7,
        [Display(Name = "Modified Surge Report")]
        SurgeModifyReport = 8,
        [Display(Name = "Deleted Digital Audience")]
        DSBDeleteSegment = 9,
        [Display(Name = "Deleted Surge Report")]
        SurgeDeleteReport = 10,
        [Display(Name = "New Content Insight Report")]
        ContentInsightCreateReport = 11,
        [Display(Name = "Modified Content Insight Report")]
        ContentInsightModifyReport = 12,
        [Display(Name = "Deleted Content Insight Report")]
        ContentInsightDeleteReport = 13,
        [Display(Name = "New Surge for Email Report")]
        SurgeEmailCreateReport = 14,
        [Display(Name = "Modified Surge for Email Report")]
        SurgeEmailModifyReport = 15,
        [Display(Name = "Deleted Surge for Email Report")]
        SurgeEmailDeleteReport = 16,
        [Display(Name = "Estimate Display Segment")]
        DSBEstimateSegment = 17,
        [Display(Name = "Estimate Surge Report")]
        SurgeEstimateReport = 18,
        [Display(Name = "Freemium")]
        Freemium = 19,
        [Display(Name = "New Label")]
        CreateLabel = 20,
        [Display(Name = "New Domain Match Report")]
        DomainMatchCreateReport = 21,
        [Display(Name = "Download Surge Report")]
        SurgeDownload = 22,
        [Display(Name = "Download Surge for Email Report")]
        SurgeEmailDownload = 23,
        [Display(Name = "Product Preview")]
        ProductPreview = 24,
        [Display(Name = "Freemium Download All Topcs")]
        FreemiumDownloadTopics = 25
    }
}
