﻿using System;
using System.Runtime.Serialization;

namespace Bombora.Integrations.Sauron.Models.Entities
{
    [DataContract]
    public class Pricing
    {
        #region constants
        public const string DSB = "dsb";
        #endregion

        public class Fields
        {
            public const string ID = "_id";
            public const string Name = "nm";
            public const string Type = "type";
            public const string Price = "price";
        }

        [DataMember]
        //[BsonId]
        public String Id { get; set; }

        [DataMember]
        //[BsonElement(Fields.Name)]
        //[BsonIgnoreIfNull]
        public String Name { get; set; }

        [DataMember]
        //[BsonElement(Fields.Type)]
        //[BsonIgnoreIfNull]
        public String EntityType { get; set; }

        [DataMember]
        //[BsonElement(Fields.Price)]
        //[BsonIgnoreIfNull]
        public double? Price { get; set; }
    }
}
