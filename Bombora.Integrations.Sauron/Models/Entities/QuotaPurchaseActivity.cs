﻿using System;

namespace Bombora.Integrations.Sauron.Models.Entities
{
    public class QuotaPurchaseActivity
    {
        public int EntityId { get; set; }
        public int PurchasedAmount { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public string ModifiedByName { get; set; }
    }
}
