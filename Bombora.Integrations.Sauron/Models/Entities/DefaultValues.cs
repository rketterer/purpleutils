﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bombora.Integrations.Sauron.Models.Entities
{
    [DataContract]
    public class DefaultValues
    {
        public class Fields
        {
            public const string ID = "_id";
            public const string Pricing = "Pricing";
            public const string Types = "Types";
            public const string Products = "Products";
        }
        [DataMember]
        //[BsonId]
        public string Id { get; set; }

        [DataMember]
        //[BsonIgnoreIfNull]
        //[BsonElement(Fields.Types)]
        public IEnumerable<String> Types { get; set; }

        [DataMember]
        //[BsonIgnoreIfNull]
        //[BsonElement(Fields.Pricing)]
        public IEnumerable<Pricing> Pricings { get; set; }

        [DataMember]
        //[BsonIgnoreIfNull]
        //[BsonElement(Fields.Products)]
        public IEnumerable<Product> Products { get; set; }
    }
}
