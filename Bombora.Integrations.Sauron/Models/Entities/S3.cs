﻿using System.Runtime.Serialization;

namespace Bombora.Integrations.Sauron.Models.Entities
{
    [DataContract]
    public class S3
    {
        public class Fields
        {
            public const string BUCKET = "bkt";
            public const string ACCESS_KEY = "a_key";
            public const string SECRET_KEY = "s_key";
            public const string IV = "iv";
            public const string REGION = "region";
        }
        [DataMember]
        //[BsonElement(Fields.BUCKET)]
        //[BsonIgnoreIfNull]
        public string Bucket { get; set; }

        [DataMember]
        //[BsonElement(Fields.REGION)]
        //[BsonIgnoreIfNull]
        public string Region { get; set; }

        [DataMember]
        //[BsonElement(Fields.ACCESS_KEY)]
        //[BsonIgnoreIfNull]
        public string AccessKey { get; set; }

        [DataMember]
        //[BsonElement(Fields.SECRET_KEY)]
        //[BsonIgnoreIfNull]
        public string SecretKey { get; set; }

        [DataMember]
        //[BsonElement(Fields.IV)]
        //[BsonIgnoreIfNull]
        public string IV { get; set; }
    }
}
