﻿using System;

namespace Bombora.Integrations.Sauron.Models.Entities
{
    public class AccountInfo
    {
        public int? Id { get; set; }
        public String Name { get; set; }
    }
}
