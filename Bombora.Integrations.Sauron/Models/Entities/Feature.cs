﻿using System;

namespace Bombora.Integrations.Sauron.Models.Entities
{
    public class Feature
    {
        public String id { get; set; }
        public String name { get; set; }
        public String description { get; set; }
        public String filename_format { get; set; }
    }
}
