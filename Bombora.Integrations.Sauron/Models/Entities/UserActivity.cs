﻿using System;

namespace Bombora.Integrations.Sauron.Models.Entities
{
    public class UserActivity
    {
        public int? EntityId { get; set; }
        public string EntityName { get; set; }
        public string EntityType { get; set; }
        public int UserId { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserEmail { get; set; }
        public EnumActions ActionId { get; set; }
        public string ActionItemId { get; set; }
        public string ActionItemName { get; set; }
        public string Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool? Deleted { get; set; }
        public string Ip { get; set; }
        public string Domain { get; set; }
        public string Topics { get; set; }
    }
}
