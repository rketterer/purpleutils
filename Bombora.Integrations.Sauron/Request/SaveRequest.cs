﻿using Bombora.Integrations.Sauron.Models.Entities;

namespace Bombora.Integrations.Sauron.Request
{
    public class SaveRequest
    {
        public int EntityId { get; set; }
        public Label UpdateLabel { get; set; }
    }
}
