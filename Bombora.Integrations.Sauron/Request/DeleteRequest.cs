﻿namespace Bombora.Integrations.Sauron.Request
{
    public class DeleteRequest
    {
        public int EntityId { get; set; }
        public int LabelId { get; set; }
    }
}
