﻿using System;

namespace Bombora.Integrations.Sauron.Request
{
    public enum Source
    {
        Other,
        Spyglass
    }
    public class AuthenticationRequest
    {
        public String Email { get; set; }

        public int UserID { get; set; }

        public String Password { get; set; }

        public Source? Source { get; set; }
    }
}
