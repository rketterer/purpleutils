﻿using Bombora.Integrations.Sauron.Models.Entities;
using Bombora.Integrations.Sauron.Response;
using Bombora.Utils.Web.RestClient;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bombora.Integrations.Sauron.RestClient
{
    public class SauronEntityClient: RestEntityCrudBase<Entity, int, EntityResponse, EntityResponse>
    {
        public SauronEntityClient(SauronConfiguration configuration) : base(configuration, "Entities")
        {
        }

        public async Task<RestEntityResponseMessage<EntityResponse>> GetEntitiesAsync()
        {
            var response = await base.GetAllAsync();
            return response;
        }        
        public async Task<RestEntityResponseMessage<EntityActivityResponse>> SelectEntityActivityAsync(int id)
        {
            var parameters = new Dictionary<string, string>
            {
                { "id", id.ToString() }
            };
            var response = await base.InvokeGetAsync<EntityActivityResponse>("Entities/SelectEntityActivity", parameters);
            return response;
        }
        public async Task<RestResponseMessage> LogUserActionAsync(UserActivity activity)
        {
            var response = await base.InvokePutAsync<UserActivity>("Entities/LogActivity", activity);
            return response;
        }
        public async Task<RestResponseMessage> LogIp2DomainQuotaPurchaseAsync(QuotaPurchaseActivity activity)
        {
            var response = await base.InvokePutAsync<QuotaPurchaseActivity>("Entities/LogIp2DomainQuotaPurchase", activity);
            return response;
        }
        public async Task<RestEntityResponseMessage<EntityActivityResponse>> GetFeaturesAsync()
        {
            var response = await base.InvokeGetAsync<EntityActivityResponse>("Entities/GetFeatures", new Dictionary<string, string>());
            return response;
        }
        public async Task<RestResponseMessage> UpdateQuotaBalance(int id, int balance)
        {
            var parameters = new Dictionary<string, string>
            {
                { "id", id.ToString() },
                { "balance", balance.ToString() }
            };
            var response = await base.InvokePutAsync("entities/UpdateQuotaBalance", parameters);
            return response;
        }
    }
}
