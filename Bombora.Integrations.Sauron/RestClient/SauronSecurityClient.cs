﻿using Bombora.Integrations.Sauron.Models.Entities;
using Bombora.Integrations.Sauron.Request;
using Bombora.Integrations.Sauron.Response;
using Bombora.Utils.Web.RestClient;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bombora.Integrations.Sauron.RestClient
{
    public class SauronSecurityClient : RestEntityCrudBase<User, int, UserResponse, UsersResponse>
    {
        public SauronSecurityClient(SauronConfiguration configuration) : base(configuration, "users")
        {
        }

        #region Users

        public async Task<RestEntityResponseMessage<UserResponse>> GetUserAsync(User user)
        {
            var parameters = new Dictionary<string, string>();
            if (user.Id != null)
                parameters.Add("id", user.Id.ToString());
            else
                parameters.Add("email", user.Email);//HttpUtility.UrlEncode(user.Email)

            var response = await base.InvokeGetAsync<UserResponse>("users", parameters);
            return response;
        }
        public async Task<RestEntityResponseMessage<UsersResponse>> GetUsersAsync()
        {
            var response = await base.GetAllAsync();
            return response;
        }
        public async Task<RestEntityResponseMessage<UsersResponse>> GetUsersAsync(int entityId)
        {
            var parameters = new Dictionary<string, string>()
            {
                { "entity_id", entityId.ToString() }
            };
            var response = await base.InvokeGetAsync<UsersResponse>("users", parameters);
            return response;
        }
        public async Task<RestEntityResponseMessage<UsersShortResponse>>  GetAllUsersAsync()
        {
            var response = await base.InvokeGetAsync<UsersShortResponse>("users/GetAllUsers", new Dictionary<string, string>());
            return response;
        }
        public async Task<RestEntityResponseMessage<UsersShortResponse>> GetNewUsersAsync(int entityId)
        {
            var parameters = new Dictionary<string, string>()
            {
                { "entity_id", entityId.ToString() }
            };
            var response = await base.InvokeGetAsync<UsersShortResponse>("users/GetNewUsers", new Dictionary<string, string>());
            return response;
        }
        public async Task<RestEntityResponseMessage<IEntityId<int>>> CreateOrEditUserAsync(User user)            
        {
            var response =  await base.CreateOrEditEntityAsync(user);
            return response;
        }
        public async Task<RestResponseMessage> DeleteUserAsync(int id)
        {            
            var response = await base.DeleteEntityAsync(id);            
            return response;
        }
        public async Task<RestEntityResponseMessage<AuthenticateResponse>> AuthenticateUserAsync(AuthenticationRequest request)
        {
            var response = await base.InvokePostAsync<AuthenticateResponse, AuthenticationRequest>("users/authenticate", request);
            return response;
        }
        #endregion

        #region Roles
        public async Task<RestEntityResponseMessage<RolesResponse>> GetRoles()
        {
            var response = await base.InvokeGetAsync<RolesResponse>("roles", new Dictionary<string, string>());
            return response;
        }
        public async Task<RestResponseMessage> AddUserRole(UserRole userRole)
        {
            var response = await base.InvokePutAsync<UserRole>("userroles", userRole);
            return response;
        }
        public async Task<RestResponseMessage> DeleteUserRole(UserRole userRole)
        {
            var parameters = new Dictionary<string, string>
            {
                { "user_id", userRole.UserId.ToString() },
                { "role_id", userRole.RoleId.ToString() },
                { "entity_id", userRole.EntityId.ToString() }
            };
            var response = await base.InvokeDeleteAsync("users", parameters);
            return response;
        }
        #endregion
    }
}
