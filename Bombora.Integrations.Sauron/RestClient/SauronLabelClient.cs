﻿using Bombora.Integrations.Sauron.Request;
using Bombora.Integrations.Sauron.Response;
using Bombora.Utils.Web.RestClient;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bombora.Integrations.Sauron.RestClient
{
    public class SauronLabelClient : RestClientBase
    {
        public SauronLabelClient(SauronConfiguration configuration) : base(configuration)
        {
            if (configuration.Timeout != null) Timeout = TimeSpan.FromMilliseconds((double)configuration.Timeout);
        }

        #region Labels
        public async Task<RestEntityResponseMessage<SaveResponse>> SaveLabel(SaveRequest request)
        {
            var response = await base.InvokePutAsync<SaveResponse, SaveRequest>("Labels", request);
            return response;
        }
        public async Task<RestResponseMessage> DeleteLabel(DeleteRequest request)
        {
            var parameters = new Dictionary<string, string>
            {
                { "entityId", request.EntityId.ToString() },
                { "labelId", request.LabelId.ToString() }
            };
            var response = await base.InvokeDeleteAsync("Labels", parameters);
            return response;
        }
        #endregion 
    }

}
