﻿using Bombora.Utils.Web.RestClient;
using System.Collections.Specialized;

namespace Bombora.Integrations.Sauron.RestClient
{
    public class SauronConfiguration : IRestClientConfiguration
    {
        public string BaseUrl { get; private set; }
        public double? Timeout { get; private set; }
        public SauronConfiguration(NameValueCollection settings) : base()
        {
            BaseUrl = string.Format(settings["Sauron.Url"],settings["Sauron.Version"]);
            Timeout = (double.TryParse(settings["Sauron.Timeout"]?.Trim(), out double timeout))? (double?) timeout : null;
        }
    }
}
