﻿using Bombora.Integrations.Sauron.Models.Entities;
using System.Collections.Generic;

namespace Bombora.Integrations.Sauron.Response
{
    public class AuthenticateResponse : ResponseBase
    {
        public long UserId { get; set; }

        public IEnumerable<Role> UserRoles { get; set; }

        public IEnumerable<int> Apis { get; set; }
    }
}
