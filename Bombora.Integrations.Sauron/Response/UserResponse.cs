﻿using Bombora.Integrations.Sauron.Models.Entities;

namespace Bombora.Integrations.Sauron.Response
{
    public class UserResponse : ResponseBase
    {
        public User User { get; set; }
    }
}
