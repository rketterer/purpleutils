﻿namespace Bombora.Integrations.Sauron.Response
{
    public class SaveResponse : ResponseBase
    {
        public int Id { get; set; }
    }
}
