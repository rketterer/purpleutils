﻿using Bombora.Integrations.Sauron.Models.Entities;
using System.Collections.Generic;

namespace Bombora.Integrations.Sauron.Response
{
    public class EntityActivityResponse : ResponseBase
    {
        public IEnumerable<UserActivity> Activity { get; set; }
    }
}
