﻿using System;

namespace Bombora.Integrations.Sauron.Response
{
    public abstract class ResponseBase
    {
        public bool Success { get; set; }
        public String Message { get; set; }
    }
}
