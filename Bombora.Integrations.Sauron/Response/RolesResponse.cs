﻿using Bombora.Integrations.Sauron.Models.Entities;
using System.Collections.Generic;

namespace Bombora.Integrations.Sauron.Response
{
    public class RolesResponse : ResponseBase
    {
        public IEnumerable<Role> Roles { get; set; }
    }
}
