﻿using Bombora.Integrations.Sauron.Models.Entities;

namespace Bombora.Integrations.Sauron.Response
{
    public class EntityResponse: ResponseBase
    {
        public Entity Entity { get; set; }
    }
}
