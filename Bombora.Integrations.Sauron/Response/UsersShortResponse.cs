﻿using Bombora.Integrations.Sauron.Models.Entities;
using System.Collections.Generic;

namespace Bombora.Integrations.Sauron.Response
{
    public class UsersShortResponse : ResponseBase
    {
        public IEnumerable<UserShort> Users { get; set; }
    }
}
