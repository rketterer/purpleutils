﻿using Bombora.Integrations.Sauron.Models.Entities;
using System.Collections.Generic;

namespace Bombora.Integrations.Sauron.Response
{
    public class UsersResponse : ResponseBase
    {
        public IEnumerable<User> Users { get; set; }
    }
}
