﻿using Bombora.Utils.Web.RestClient;

namespace Bombora.Integrations.Sauron.Response
{
    public class NewIdResponse : ResponseBase, IEntityId<int>
    {
        public int Id { get; set; }
    }
}
