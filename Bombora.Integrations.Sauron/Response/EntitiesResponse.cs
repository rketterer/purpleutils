﻿using Bombora.Integrations.Sauron.Models.Entities;
using System.Collections.Generic;

namespace Bombora.Integrations.Sauron.Response
{
    public class EntitiesResponse
    {
        public IEnumerable<Entity> Entities { get; set; }
    }
}
