﻿using Bombora.Integrations.Sauron.Models.Entities;
using System.Collections.Generic;

namespace Bombora.Integrations.Sauron.Response
{
    public class FeaturesResponse : ResponseBase
    {
        public IEnumerable<Feature> Features { get; set; }
    }
}
