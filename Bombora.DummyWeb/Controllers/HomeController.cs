﻿using Bombora.Integrations.Sauron.RestClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Bombora.DummyWeb.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()
        {
            var sauronClient = new SauronEntityClient(new SauronConfiguration(ConfigurationManager.AppSettings));
            var response = await sauronClient.GetEntityAsync(50067);            
            var entityResponse = await response.GetEntity();
            var surgeProduct = entityResponse.Entity?.Products.FirstOrDefault(p => p.Id.Equals("surge"));
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
    }
}