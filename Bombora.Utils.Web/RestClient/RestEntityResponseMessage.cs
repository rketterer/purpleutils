﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Bombora.Utils.Web.RestClient
{
    public class RestEntityResponseMessage<TResponseEntity> : RestResponseMessage
    {
        private TResponseEntity responseEntity;

        public RestEntityResponseMessage(HttpResponseMessage responseMessage) : base(responseMessage)
        {}

        public async Task<TResponseEntity> GetEntity()
        {
            if (this.Success)
                responseEntity = await HttpContentExtensions.ReadAsAsync<TResponseEntity>(Message.Content);
            return responseEntity;
        }
        public void SetEntity(TResponseEntity responseEntity)
        {
            this.responseEntity = responseEntity;
        }
    }
}
