﻿using System.Net.Http;

namespace Bombora.Utils.Web.RestClient
{
    public class RestResponseMessage
    {
        public HttpResponseMessage Message { get; private set; }
        public bool Success => Message?.IsSuccessStatusCode ?? false;
        public RestResponseMessage(HttpResponseMessage responseMessage)
        {
            Message = responseMessage;
        }
    }
}
