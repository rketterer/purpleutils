﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bombora.Utils.Web.RestClient
{
    public class RestEntityCrudBase<TEntity,TEntityId, TEntitiyResponse,TEntitiyCollectionResponse> : RestClientBase
    {
        protected readonly string entityEndpoint;
        public RestEntityCrudBase(IRestClientConfiguration config, string entityEndpoint): base(config)
        {
            this.entityEndpoint = entityEndpoint;
        }

        #region CRUD operations
        public virtual async Task<RestEntityResponseMessage<TEntitiyResponse>> GetEntityAsync(TEntityId id, string idParameterName = @"id")
        {
            var parameters = new Dictionary<string, string>
            {
                { idParameterName, id.ToString() }
            };
            var response = await base.InvokeGetAsync<TEntitiyResponse>(entityEndpoint, parameters);
            return response;
        }
        public virtual async Task<RestEntityResponseMessage<TEntitiyCollectionResponse>> GetAllAsync()
        {
            var response = await base.InvokeGetAsync<TEntitiyCollectionResponse>(entityEndpoint, new Dictionary<string, string>());
            return response;
        }
        public virtual async Task<RestEntityResponseMessage<IEntityId<TEntityId>>> CreateOrEditEntityAsync(TEntity entity)
        {
            var response = await base.InvokePutAsync<IEntityId<TEntityId>, TEntity>(entityEndpoint, entity);
            return response;
        }
        public virtual async Task<RestResponseMessage> DeleteEntityAsync(int id)
        {
            var parameters = new Dictionary<string, string>
            {
                { "id", id.ToString() }
            };
            var response = await base.InvokeDeleteAsync(entityEndpoint, parameters);
            return response;
        }
        #endregion
    }
}
