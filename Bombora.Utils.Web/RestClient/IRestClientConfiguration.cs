﻿namespace Bombora.Utils.Web.RestClient
{
    public interface IRestClientConfiguration
    {
        string BaseUrl { get; }
    }
}
