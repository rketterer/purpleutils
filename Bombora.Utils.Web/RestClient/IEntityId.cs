﻿namespace Bombora.Utils.Web.RestClient
{
    public interface IEntityId<TEntityId> 
    {
        TEntityId Id { get; set; }
    }
}
