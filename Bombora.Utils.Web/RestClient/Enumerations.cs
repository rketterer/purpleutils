﻿namespace Bombora.Utils.Web.RestClient
{
    public enum Format
    {
        Xml,
        Json
    }
}
