﻿using Bombora.Utils.Web.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Bombora.Utils.Web.RestClient
{
    public abstract class RestClientBase : HttpClient
    {
        protected Serializer serializer = new Serializer();
        public Format RequestFormat { get; set; }

        #region BuildQueryString
        private string BuildQueryString(IDictionary<string, string> parameters)
        {
            var queryString = string.Join(@"&", parameters.Select(param => $"{param.Key}={Uri.EscapeDataString(param.Value)}"));
            return $"?{queryString}";
        }

        private string BuildQueryString(IEnumerable<string> parametersValues)
        {
            var queryString = string.Join(@"/", parametersValues.Select(p => Uri.EscapeDataString(p)));
            return $@"/{queryString}";
        }
        #endregion BuildQueryString

        #region Headers
        public void AddAuthenticationHeader(string name, string value)
        {
            if (!(string.IsNullOrEmpty(name) || string.IsNullOrEmpty(value)))
                base.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(name, value);
        }
        #endregion

        #region Build Payload

        private MediaTypeFormatter FormatterFactory(Format format)
        {
            switch (format)
            {
                case Format.Xml: return new XmlMediaTypeFormatter();
                default: return new JsonMediaTypeFormatter();
            }
        }

        private HttpContent BuildPayload<TRequest>(TRequest requestPayload)
        {
            string strPayload = serializer.Serialize<TRequest>(FormatterFactory(RequestFormat), requestPayload);
            var content = new StringContent(strPayload, Encoding.UTF8, "application/json");
            return content;
        }

        #endregion Build Payload               

        public RestClientBase(IRestClientConfiguration config)
        {
            BaseAddress = new Uri(config.BaseUrl);
        }

        #region Invoke Get
        protected async Task<RestResponseMessage> InvokeGetAsync(string operationName, IEnumerable<string> parametersValues)
        {
            var response = await base.GetAsync($"{operationName}{BuildQueryString(parametersValues)}");
            return new RestResponseMessage(response);
        }
        protected async Task<RestEntityResponseMessage<TResponse>> InvokeGetAsync<TResponse>(string operationName, IEnumerable<string> parametersValues)
        {
            var response = await this.InvokeGetAsync(operationName, parametersValues);
            return new RestEntityResponseMessage<TResponse>(response.Message);
        }
        protected async Task<RestResponseMessage> InvokeGetAsync(string operationName, IDictionary<string, string> parameters)
        {
            var response = await base.GetAsync($"{operationName}{BuildQueryString(parameters)}");
            return new RestResponseMessage(response);
        }
        protected async Task<RestEntityResponseMessage<TResponse>> InvokeGetAsync<TResponse>(string operationName, IDictionary<string, string> parameters)
        {
            var response = await this.InvokeGetAsync(operationName, parameters);
            return new RestEntityResponseMessage<TResponse>(response.Message);
        }
        #endregion

        #region Invoke Post
        protected async Task<RestResponseMessage> InvokePostAsync(string operationName, IDictionary<string, string> parameters)
        {
            using (var payload = new FormUrlEncodedContent(parameters))
            {
                var response = await base.PostAsync(operationName, payload);
                return new RestResponseMessage(response);
            }
        }
        protected async Task<RestEntityResponseMessage<TResponse>> InvokePostAsync<TResponse>(string operationName, IDictionary<string, string> parameters)
        {
            var response = await this.InvokePostAsync(operationName, parameters);
            return new RestEntityResponseMessage<TResponse>(response.Message);
        }

        protected async Task<RestResponseMessage> InvokePostAsync<TRequest>(string operationName, TRequest requestPayload)
        {
            var response = await base.PostAsync(operationName, this.BuildPayload<TRequest>(requestPayload));
            return new RestResponseMessage(response);
        }
        protected async Task<RestEntityResponseMessage<TResponse>> InvokePostAsync<TResponse, TRequest>(string operationName, TRequest requestPayload)
        {
            var response = await this.InvokePostAsync(operationName, requestPayload);
            return new RestEntityResponseMessage<TResponse>(response.Message);
        }
        #endregion

        #region Invoke Put
        protected async Task<RestResponseMessage> InvokePutAsync(string operationName, IDictionary<string, string> parameters)
        {
            using (var payload = new FormUrlEncodedContent(parameters))
            {
                var response = await base.PutAsync(operationName, payload);
                return new RestResponseMessage(response);
            }
        }
        protected async Task<RestEntityResponseMessage<TResponse>> InvokePutAsync<TResponse>(string operationName, IDictionary<string, string> parameters)
        {
            var response = await this.InvokePutAsync(operationName, parameters);
            return new RestEntityResponseMessage<TResponse>(response.Message);
        }

        protected async Task<RestResponseMessage> InvokePutAsync<TRequest>(string operationName, TRequest requestPayload)
        {
            var response = await base.PutAsync(operationName, this.BuildPayload<TRequest>(requestPayload));
            return new RestResponseMessage(response);
        }
        protected async Task<RestEntityResponseMessage<TResponse>> InvokePutAsync<TResponse, TRequest>(string operationName, TRequest requestPayload)
        {
            var response = await this.InvokePutAsync(operationName, requestPayload);
            return new RestEntityResponseMessage<TResponse>(response.Message);
        }
        #endregion

        #region Invoke Delete
        protected async Task<RestResponseMessage> InvokeDeleteAsync(string operationName, IEnumerable<string> parametersValues)
        {
            var response = await base.DeleteAsync($"{operationName}{BuildQueryString(parametersValues)}");
            return new RestResponseMessage(response);
        }
        protected async Task<RestResponseMessage> InvokeDeleteAsync(string operationName, IDictionary<string, string> parameters)
        {
            var response = await base.DeleteAsync($"{operationName}{BuildQueryString(parameters)}");
            return new RestResponseMessage(response);
        }
        #endregion
    }
}
