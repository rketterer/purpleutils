﻿using Bombora.Utils.Web.RestClient;
using System.Collections.Specialized;

namespace Bombora.Integrations.LinkedIn.RestClient
{
    public class LinkedInConfiguration : IRestClientConfiguration
    {
        public string BaseUrl { get; private set; }
        public string ClientId { get; private set; }
        public string ClientSecret { get; private set; }
        public string RedirectUri { get; private set; }
        public string AccessTokenEndPoint { get; private set; }
        public LinkedInConfiguration(NameValueCollection settings)
        {
            BaseUrl = settings["LinkedIn.BaseUrl"];
            ClientId = settings["LinkedIn.ClientId"];
            ClientSecret = settings["LinkedIn.ClientSecret"];
            RedirectUri = settings["LinkedIn.RedirectUri"];
            AccessTokenEndPoint = settings["LinkedIn.AccessTokenEndPoint"];
        }

        
    }
}
