﻿using Bombora.Integrations.LinkedIn.Responses;
using Bombora.Utils.Web.RestClient;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bombora.Integrations.LinkedIn.RestClient
{
    public class LinkedInApiClient : RestClientBase
    {
        private LinkedInApiConfiguration configuration;
        public LinkedInApiClient(LinkedInApiConfiguration configuration) : base(configuration)
        {
            this.configuration = configuration;
        }

        public async Task<RestEntityResponseMessage<ElementResponse>> GetAdAccountAsync(string token)
        {
            try
            {
                base.AddAuthenticationHeader("Bearer", token);
                var parameters = new Dictionary<string, string>{ { "q", "search" } };
                var response = await base.InvokeGetAsync<ElementResponse>("adAccountsV2", parameters);
                return response;
            }
            catch (Exception e)
            {
                throw new Exception($"{e.Message}");
            }
        }

        public async Task<RestEntityResponseMessage<ElementResponse>> GetSegmentByAccountAsync(string account, string token)
        {
            try
            {
                base.AddAuthenticationHeader("Bearer", token);
                var parameters = new Dictionary<string, string>
                {
                    { "q", "account" },
                    { "account", account }
                };
                var response = await base.InvokeGetAsync<ElementResponse>("dmpSegments",parameters);
                return response;
            }
            catch (Exception e)
            {
                throw new Exception($"{e.Message}");
            }
        }
    }
}
