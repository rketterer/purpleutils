﻿using Bombora.Integrations.LinkedIn.Responses;
using Bombora.Utils.Web.RestClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Bombora.Integrations.LinkedIn.RestClient
{
    public class LinkedInClient : RestClientBase
    {
        private LinkedInConfiguration configuration;
        private Dictionary<string, string> defaultParameters;

        private async Task<AccessTokenResponse> CallAccessTokenEndpointAsync(IDictionary<string, string> parameters)
        {
            try
            {
                var response = await base.InvokePostAsync<AccessTokenResponse>(configuration.AccessTokenEndPoint, parameters);
                return await response?.GetEntity();
            }
            catch (Exception e)
            {
                throw new Exception($"{e.Message}");
            }
        }
        private Dictionary<string, string> BuildParameters(KeyValuePair<string, string> parameter, string[] exclude = null)
        {
            var parameters  = defaultParameters.Concat(new List<KeyValuePair<string, string>>{parameter})
                .ToDictionary(item => item.Key, item => item.Value);
            return (exclude == null) ? parameters 
                : parameters.Where(p => !exclude.Contains(p.Key)).ToDictionary(item => item.Key, item => item.Value);
        }

        public LinkedInClient(LinkedInConfiguration configuration): base(configuration)
        {
            this.configuration = configuration;
            defaultParameters = new Dictionary<string, string>
            {
                { "grant_type", "authorization_code" },
                { "client_id", configuration.ClientId},
                { "client_secret", configuration.ClientSecret},
                { "redirect_uri", configuration.RedirectUri }
            };
        }

        public async Task<AccessTokenResponse> GetAccessTokenAsync(string code)
        {
            try
            {
                var parameters = BuildParameters(new KeyValuePair<string, string>("code", code));
                return await CallAccessTokenEndpointAsync(parameters);
            }
            catch (Exception e)
            {
                throw new Exception($"{e.Message}");
            }
        }
        public async Task<AccessTokenResponse> RefreshAccessTokenAsync(string refreshAccessToken)
        {
            try
            {
                var parameters = BuildParameters(new KeyValuePair<string, string>("refresh_token", refreshAccessToken)
                    ,new string[] { "redirect_uri" });
                return await CallAccessTokenEndpointAsync(parameters);
            }
            catch (Exception e)
            {
                throw new Exception($"{e.Message}");
            }
        }
        public async Task<RestEntityResponseMessage<int>> CreateSegment(string request, string token)
        {
            var strSegmentId = String.Empty;
            IEnumerable<string> values;
            try
            {
                base.AddAuthenticationHeader("Bearer", token);
                var content = new StringContent(request, System.Text.Encoding.UTF8, "application/json");
                var response = await base.InvokePostAsync<int, StringContent>("dmpSegments", content);
                var segmentId = 0;
                if (response.Message.Headers.TryGetValues("X-LinkedIn-Id", out values))
                {
                    strSegmentId = values.First();                    
                    Int32.TryParse(strSegmentId, out segmentId);
                    response.SetEntity(segmentId);
                }

                return response;
            }
            catch (Exception e)
            {
                throw new Exception($"{e.Message}");
            }
        }
    }
}
