﻿using Bombora.Utils.Web.RestClient;

namespace Bombora.Integrations.LinkedIn.RestClient
{
    public class LinkedInApiConfiguration: IRestClientConfiguration
    {
        public string BaseUrl { get; private set; }
        public LinkedInApiConfiguration(string baseUrl): base()
        {
            BaseUrl = baseUrl;
        }
    }
}
