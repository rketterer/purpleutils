﻿using System.Runtime.Serialization;

namespace Bombora.Integrations.LinkedIn.Responses
{
    [DataContract]
    public class AdAccountResponse
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "id")]
        public int Id { get; set; }

    }
}
