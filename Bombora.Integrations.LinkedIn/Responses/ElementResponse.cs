﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Bombora.Integrations.LinkedIn.Responses
{
    [DataContract]
    public class ElementResponse
    {
        [DataMember(Name = "elements")]
        public List<AdAccountResponse> Elements { get; set; }
    }
}
