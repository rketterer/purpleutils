﻿using System.Runtime.Serialization;

namespace Bombora.Integrations.LinkedIn.Responses
{
    [DataContract]
    public class AccessTokenResponse
    {
        [DataMember(Name = "userId")]
        public int UserId { get; set; }

        [DataMember(Name = "access_token")]
        public string AccessToken { get; set; }

        [DataMember(Name = "expires_in")]
        public int ExpiresIn { get; set; }

        [DataMember(Name = "refresh_token")]
        public string RefreshToken { get; set; }

        [DataMember(Name = "refresh_token_expires_in")]
        public int RefreshTokenExpiresIn { get; set; }

        [DataMember(Name = "is_mid_market")]
        public bool? IsMidMarket { get; set; }

        public override bool Equals(object obj)
        {
            var response = obj as AccessTokenResponse;
            return response != null &&
                   UserId == response.UserId &&
                   AccessToken == response.AccessToken &&
                   ExpiresIn == response.ExpiresIn &&
                   RefreshToken == response.RefreshToken &&
                   RefreshTokenExpiresIn == response.RefreshTokenExpiresIn;
        }
    }
}
